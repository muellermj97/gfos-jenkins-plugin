/**
 *  Globaler Event Listener
 */
$(document).ready(function() {
    
    /**
     *  Setting-Event zum speichern der ServerIP
     */
    $("body").delegate("#settings_save_server", "click", function() {
        
        //Speichern
        var serverIP = $("#settings_server_ip").val();
        settings_setValue("serverIP", serverIP);
        
        refresh(); // Überprüft die Verbindung
        $("#settings_save_server").addClass("success");
    });
    
    /**
     *  Setting-Event zum speichern des Aktualisierungsintervalls
     */
    $("body").delegate("#settings_save_refresh", "click", function() {
        
        //Speichern
        var refrehInterval = $("#settings_refresh_interval").val();
        settings_setValue("refreshInterval", refrehInterval);
        
        chrome.runtime.reload(); // Startet die Extension neu
        $("#settings_save_refresh").addClass("success");
    });
    
    /**
     *  Event zum anzeigen des mit dem Slider ausgewählten Wertes
     */
    $("body").delegate("#settings_refresh_interval", "input", function() {
        var interval = $("#settings_refresh_interval").val();

        $("#intervalOut").text(interval);
    });
    
    
    /**
     *  Event zum Laden der Builds eines Jobs
     */
    $("body").delegate(".job", "click", function() {
        var jobName = $(this).data("name");
        
        //Merke den Namen des angeklickten Jobs, damit der ViewLoader diesen verwenden kann.
        settings_setValue("jobName", jobName);
        
        //Lade das Job-Template
        loadView("job");
    });
    
    /**
     *  Die folgenden 2 Vorgänge sind sich sehr ähnlich. Hier is der Unterschied:
     *  [Jobliste] -> Klick des Pfeils auf der Startseite
     *  [Jobdetails] -> Klick des Buttons auf der Job-Seite
     */
    
    /**
     *  Event zum Laden eines neuen Tabs mit der Jenkinsseite des Jobs [Jobliste]
     */
    $("body").delegate(".job-newTab", "click", function() {        
        var jobName = $(this).parent().data("name");
        
        //Hol die gespeicherte URL
        jenkins_getURL(function(serverURL) {
                
            //Ergänze die URL, so dass sie zum Job führt
            var url = serverURL + "job/" + jobName;
            
            //Öffne den neuen Tab
            chrome.tabs.create({url: url});
        });
        
    });
    
    /**
     *  Event zum Laden eines neuen Tabs mit der Jenkinsseite des Jobs [Jobdetails]
     */
    $("body").delegate("#job_newTab", "click", function() {        
        var jobName = $(this).data("name");
        
        //Hol die gespeicherte URL
        jenkins_getURL(function(serverURL) {
                
            //Ergänze die URL, so dass sie zum Job führt
            var url = serverURL + "job/" + jobName;
            
            //Öffne den neuen Tab
            chrome.tabs.create({url: url});
        });
        
    });
    
    /**
     *  Event zum Starten eines neuen Builds
     */
    $("body").delegate("#job_build", "click", function() {        
        var jobName = $(this).data("name");
        
        jenkins_startBuild(jobName, function(result, data) {
            if (result) {
                //Anfrage war erfolgreich
                
                reloadView();
            } else if (data.status == 403) {
                //Server lehnte die Anfrage ab
                
                $("#job_build_error").text("Build-Anfrage abgelehnt (#403)");
            }
        });
        
    });
    
    /**
     *  Event zum Laden eines neuen Tabs mit der Jenkinsseite des Builds
     */
    $("body").delegate(".build", "click", function() {        
        var jobName = $(this).data("job");
        var number = $(this).data("number");
        
        //Hol die URL
        jenkins_getURL(function(serverURL) {
                
            //Ergänze die URL, so dass sie zum Build führt
            var url = serverURL + "job/" + jobName + "/" + number;
            
            //Öffne den neuen Tab
            chrome.tabs.create({url: url});
        });
    });
    
    
    /*
     *  Jenkins Startseite im neuen Tab öffnen
     */
    $("body").delegate("#jenkins_new_tab", "click", function() {
        
        //Hol die URL
        jenkins_getURL(function(serverURL) {
            
            //Öffne den neuen Tab
            chrome.tabs.create({url: serverURL}); 
        });
    });
});