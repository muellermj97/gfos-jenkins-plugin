/**
 *  Skript mit verschiedenen Funktionen, aufgeteilt in 3 Bereiche
 *
 *  [PART I]    ->  Refresh-Cycle
 *                  Funktionen, die mit dem Refresh-Cycle des Plugins zu tun haben
 *
 *  [PART II]   ->  onViewLoad-Events
 *                  Diese werden ausgeführt, wenn eine View im HTML Code Bezug auf diese nimmt
 *                  So kann direkt nach Laden der View eine bestimmte Funktion ausgeführt werden,
 *                  z.B., um die Liste mit Jobs zu laden
 *
 *  [PART III]  ->  Jenkins-API
 *                  Selbstgeschriebe API, um den Zugriff auf Jenkins zu erleichtern.
 *
 *  [PART IV]   ->  Utilities
 *                  Funktionen, die z.B. das Arbeiten mit Strings erleichtern
 *
 */


// ============================================================================

/**
 *  [PART I]    ->  Refresh-Cycle
 */

/**
 *  refresh() : function
 *  => Prüft die Verbindung zum Server und lädt, falls erfolgreich, die Ansicht neu
 *
 *  @param startup : boolean
 *  => Wenn die Extension gerade erst gestartet
 */
function refresh(startup) {
    
    //Wenn "startup" nicht übergeben wurde, wird als Standard false verwendet
    startup = typeof startup !== 'undefined' ? startup : false;
    
    //Teste die Verbindung
    jenkins_connect(function(success) {
        
        if (success) {
            //Verbindung war erfolgreich
            
            //Zeige den Erfolg in der Navbar an
            $(".navbar-connection").text("Verbunden");
            $(".navbar-connection").attr("data-status", "connected");
            
            //Wenn die Extension nicht gerade erst gestartet wurde, soll
            //die Anzeige auch aktualisiert werden.
            if (!startup)
                reloadView();
            
        } else {
            //Verbindung schlug fehl
            
            //In der Navbar anzeigen
            $(".navbar-connection").text("Nicht verbunden");
            $(".navbar-connection").attr("data-status", "not-connected");
        }
    });
}

/**
 *  reloadView() : function
 *  => Erneuert die HTML-Darstellung (Prüft nicht (!), ob die Verbindung noch besteht.
 */
function reloadView() {
    
    //Hol die zuletzt angezeigte Ansicht
    settings_getValue("lastView", "home", function(lastView) {
        
        //Ausnahmen, bei denen ein Neuladen überflüssig oder sogar
        //unerwünscht ist
        var exceptions = [
            "settings",
            "markup",
            "legende"
        ]
        
        //Wenn also keine Ausnahme vorliegt, Ansicht neu laden
        if (exceptions.indexOf(lastView) == -1) {
            loadView(lastView);
        }
    });
}


// ============================================================================


/**
 *  [PART II]    ->  onViewLoad-Events
 */

/*
 *  Wenn die Einstellungsseite geladen wurde
 */
function onSettingsLoad() {
    
    //Zeig den aktuellen Wert der URL an
    settings_getValue("serverIP", "http://localhost:8080", function(serverIP) {
        $("#settings_server_ip").val(serverIP);
    });
    
    //Zeig den aktuellen Wert des RefreshIntervals an
    settings_getValue("refreshInterval", "60", function(refreshInterval) {
        $("#intervalOut").text(refreshInterval);    //In Textform
        $("#settings_refresh_interval").val(refreshInterval);   //Im Slider
    });
}

/*
 *  Wenn die Startseite geladen wurde
 */
function onHomeLoad() {
    
    //Hol die Liste mit Jobs vom Server
    jenkins_getJobs(function (data) {
        
        //Loop durch die Liste
        for (var i = 0; i<data.length; i++) {
            var obj = data[i];
            
            /*
             * Damit der Name korrekt angezeigt werden kann, wird
             * er auf 28 Zeichen runter gekürzt
             */
            var jobNameDisplay = obj.name;
            if (jobNameDisplay.length > 28)
                jobNameDisplay = jobNameDisplay.substring(0, 28) + "...";
            
            //Anzeigen
            var html = '<div class="job" title="{0}" data-name="{0}"><div class="job-color" data-status="{1}">&nbsp;</div><span class="job-title">{2}</span><img src="src/newTab-Icon.png" class="job-newTab" title="Im neuen Tab &ouml;ffnen" height="20px" width="20px"></img></div>'
            
            //{0} => obj.name
            //{1} => obj.color
            //{2} => jobNameDisplay
            
            $("#list_jobs").append(html.format(obj.name, obj.color, jobNameDisplay));
            
        }
        
    });
    
}

/*
 *  Wenn ein Job angezeigt werden soll
 */
function onJobLoad() {
    
    //Hol den Namen des Jobs, der vor dem Laden hätte abgespeichert werden sollen
    settings_getValue("jobName", "undefined", function(jobName) {

        //Namen anzeigen
        $("#ji_Name").text(jobName);
        
        //Buttons mit Meta-Daten versehen
        $("#job_newTab").attr("data-name", jobName);
        $("#job_build").attr("data-name", jobName);
                
        //Lade weitere Infos über den Job
        jenkins_getJob(jobName, function(data) {
        
            //Zeige die Beschreibung an, sofern eine vorhanden ist
            var description = data.description;
            if (description.length > 0)
                $("#ji_Description").html(description.replaceLineBreaks());
            else
                $("#ji_Description").text("Keine Beschreibung vorhanden.");
            
            //Build-Stabilität
            $("#ji_HealthReport_Description").text(data.healthReport[0].description);   //Als Text
            $("#ji_HealthReport_Score").text(data.healthReport[0].score + "%");         //In Prozent
            
            //Array mit Buildnummern
            var builds = data.builds;

            //Loop durch die Builds
            for (var i=0; i<builds.length; i++) {
                var obj = builds[i];
                
                //Die Elemente zur Darstellung eines Builds werden erzeugt, einige Daten fehlen aber noch
                var html = '<div class="build" data-job="{0}" data-number="{1}"><div class="build-color" data-status="">&nbsp;</div> <span class="build-name">{1}. Build</span><span class="build-date"></span><span class="build-duration"></span></div>'
                
                //{0} => jobName
                //{1} => obj.number
                $("#list_builds").append(html.format(jobName, obj.number));
                
                //Mehr Infos über die jeweiligen Builds
                jenkins_getBuild(jobName, obj.number, function(json) {
                    
                    //Dauer des Builds wird geholt und auf Sekunden mit zwei Nachkommastellen gerundet
                    var duration = Math.round((json.duration / 1000) * 100) / 100;
                    
                    //Das Datum wird als Timestamp geholt und in ein reguläres Datumsobjekt umgewandelt
                    var buildDate = new Date(json.timestamp);
                    
                    //Das Datumsobjekt wird ausgewertet
                    var date = buildDate.getDate(buildDate) + "." + (buildDate.getMonth(buildDate) + 1) +  "." + buildDate.getFullYear(buildDate);
                    
                    //Query-Selector, um das HTML Element des Builds einfacher anzusprechen
                    var buildSelector = ".build[data-number='" + json.number + "']";
                    
                    //Zeige den Status des Builds als Farbe an
                    $(buildSelector + " .build-color").attr("data-status", json.result);
                    
                    //Zeige das Datum des Builds an
                    $(buildSelector + " .build-date").text("Vom: " + date + "");
                    
                    //Zeige die Dauer an
                    if (duration < 10) {
                        
                        //Weniger als 10 Sekunden, wird mit 2 Nachkommastellen angezeigt
                        $(buildSelector + " .build-duration").text("Builddauer: " + duration + " sec.");
                    }else if (duration < 60) {
                        
                        //Weniger als 60 Sekunden, wird ohne Nachkommastellen angezeigt
                        duration = Math.floor(duration);
                        $(buildSelector + " .build-duration").text("Builddauer: " + duration + " sec.");
                    }else {
                        
                        //Über 1 Minute, wird gerundet in Minuten angezeigt
                        duration = Math.floor(duration/60);
                        $(buildSelector + " .build-duration").text("Builddauer: " + duration + " min.");
                    }
                });
            }
        });
        
    });
}


// ============================================================================


/**
 *  [PART III]  ->  Jenkins-API
 */

/**
 *  jenkins_getURL() : function
 *  => Gibt die URL des Servers zurück
 *
 *  @param callback : function
 *  Eine Funktion, die das Resultat benutzt und auswertet
 *  Ermöglicht eine Asynchrone Verwendung der Daten
 */
var jenkins_getURL = function(callback) {
    
    //Hol die URL aus den Einstellungen
    settings_getValue("serverIP", "http://localhost:8080", function(serverIP) {
        
        //Slash am Ende vereinheitlichen
        if (!serverIP.endsWith("/"))
            serverIP += "/";
        
        //Sicherstellen, dass das HTTP Protokoll (oder https) vorab steht
        if (!serverIP.startsWith("http://") && !serverIP.startsWith("https://"))
            serverIP = "http://" + serverIP;
        
        //An die Callback-Funktion weitergeben
        callback(serverIP);
    });
}

/**
 *  jenkins_connect() : function
 *  => Prüft die Verbindung zum Server
 *
 *  @param callback : function
 *  Eine Funktion, die das Resultat benutzt und auswertet
 *  Ermöglicht eine Asynchrone Verwendung der Daten
 */
var jenkins_connect = function(callback) {
    
    //Hol die URL
    jenkins_getURL(function(serverIP) {
        
        //Führe eine AJAX Anfrage aus
        $.ajax({
            url: serverIP,
        }).done(function(data) {
            //Server ist ansprechbar
            
            callback(true);
        }).fail(function() {
            //Verbindung fehlgeschlagen
            
            callback(false);
        });
        
    });
    
}

/**
 *  jenkins_getJobs() : function
 *  => Gibt die Liste mit Jobs zurück
 *  
 *  @param callback : function
 *  Eine Funktion, die das Resultat benutzt und auswertet
 *  Ermöglicht eine Asynchrone Verwendung der Daten
 */
var jenkins_getJobs = function(callback) {

    //Erst Verbindung prüfen
    jenkins_connect(function(connected) {
        
        if (connected) {
            //Verbindung erfolgreich
            
            //URL holen
            jenkins_getURL(function(serverURL) {
                
                //Zur API ergänzen
                var apiURL = serverURL + "api/json";
                
                //Über AJAX die Daten abfragen
                $.ajax({
                    url: apiURL
                }).done(function(data) {
                    //Daten erhalten
                    var jobs = data.jobs;
                    
                    //An Callback-Funktion weitergeben
                    callback(jobs);
                }).fail(function() {
                    //Verbindung fehlgeschlagen
                    callback(false);
                });
                
            });
            
        } else {
            //Keine Verbindung zum Server
            callback(false);
        }        
    });    
}

/**
 *  jenkins_getJob() : function
 *  => Gibt Infos über einen bestimmten Job zurück
 *
 *  @param jobName : string
 *  => Name des Jobs
 * 
 *  @param callback : function
 *  Eine Funktion, die das Resultat benutzt und auswertet
 *  Ermöglicht eine Asynchrone Verwendung der Daten
 */
var jenkins_getJob = function(jobName, callback) {
    
    //Erst Verbindung zum Server prüfen
    jenkins_connect(function(connected) {
        
        if (connected) {
            //Verbindung erfolgreich
            
            //URL holen...
            jenkins_getURL(function(serverURL) {
                
                //...und zur API des passenden Jobs ergänzen
                var apiURL = serverURL + "job/" + jobName + "/api/json";
                
                //AJAX-Anfrage durchführen
                $.ajax({
                    url: apiURL
                }).done(function(data) {
                    //Daten erhalten
                    
                    //An die Callback-Funktion weitergeben
                    callback(data);
                }).fail(function() {
                    //AJAX-Anfrage fehlgeschlagen
                    
                    callback(false);
                });
            });
            
        } else {
            //Verbindung schlug fehl
            callback(false);
        } 
    });    
}

/**
 *  jenkins_getBuild() : function
 *  => Gibt Infos über einen bestimmten Build zurück
 *
 *  @param jobName : string
 *  => Name des Jobs, zu dem der Build gehört
 *
 *  @param buildNumber : int
 *  => Nummer des Builds
 *
 *  @param callback : function
 *  Eine Funktion, die das Resultat benutzt und auswertet
 *  Ermöglicht eine Asynchrone Verwendung der Daten
 */
var jenkins_getBuild = function(jobName, buildNumber, callback) {
    
    //Erst Verbindung überprüfen
    jenkins_connect(function(connected) {
        
        if (connected) {
            //Verbindung erfolgreich
            
            //URL holen
            jenkins_getURL(function(serverURL) {
                
                //Und zur API des Builds ergänzen
                var apiURL = serverURL + "job/" + jobName + "/" + buildNumber + "/api/json";
                
                //AJAX-Request durchführen
                $.ajax({
                    url: apiURL
                }).done(function(data) {
                    //Daten erhalten
                    
                    //und weitergeben
                    callback(data);
                }).fail(function() {
                    //Verbindung fehlgeschlagen
                    callback(false);
                });
            });
        } else {
            //Keine Verbindung zum Server
            callback(false);
        }
    });
}

var jenkins_startBuild = function(jobName, callback) {
    
    //Hol die URL
    jenkins_getURL(function(serverURL) {
        
        //Ergänze die URL; so dass sie zum Build führt und diesen startet
        var apiURL = serverURL + "job/" + jobName + "/build?delay=0sec";
            
        //Führe die Buildanforderung mittels AJAX aus
        $.ajax({
            url: apiURL
        }).done(function(data) {
            //Anfrage erfolgreich abgeschickt, Antwort zurückbekommen
            
            callback(true);
        }).fail(function(data) {
            //Anfrage fehlgeschlagen (z.B. HTTP-Error #403)
            
            callback(false, data);
        });
    });
}


// ============================================================================


/**
 * [PART IV]   ->  Utilities
 */

/**
 *  String.endsWith() : function
 *  => Prüft, ob das String-Objekt auf ein bestimmtes Pattern endet
 *
 *  @param pattern : string
 */
String.prototype.endsWith = function(pattern) {
    var d = this.length - pattern.length;
    return d >= 0 && this.lastIndexOf(pattern) === d;
}

/**
 *  String.startsWith() : function
 *  => Prüft, ob das String-Objekt mit einem bestimmten Pattern anfängt
 *
 *  @param pattern : string
 */
String.prototype.startsWith = function(pattern) {
    return this.indexOf(pattern) === 0;
}

/**
 *  String.replaceLineBreaks() : function
 *  => Ersätzt \n mit dem HTML-Tag <br>, so dass der String in HTML korrekt angezeigt wird
 *  => Vergleichbar mit PHPs nl2br()
 */
String.prototype.replaceLineBreaks = function() {
    return this.replace("\n", "<br>");
}

/**
 *  String.format() : function
 *  => Formatiert den String entsprechend folgendem Beispiels:
 *  
    var myString = "My name is {0} and I am {1} years old. I live in {2}, {3}, {4}.";
    console.log(myString.format("Heinrich", "42", "Essen", "NRW", "Germany"));
    
    ==> My name is Heinrich and I am 42 years old. I live in Essen, NRW, Germany.
 */
String.prototype.format = function() {
    
    var result = this;
    
    for (var i=0; i < arguments.length; i++) {
        var regEx = new RegExp("\\{" + (i) + "\\}", "gm");
        
        result = result.replace(regEx, arguments[i]);
    }
    
    return result;
}