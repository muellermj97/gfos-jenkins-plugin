/**
 *  Funktion zum Laden einer Seite in den Container der Extension
 *  Parameter: "view" = Name der anzuzeigenden Seite
 */
function loadView(view) {
	
    $("#container").load(chrome.extension.getURL("views/" + view + ".html"), function (response, status, xhr)       {
            if (status == "error") {
                loadView ("home"); // TODO: Wenn Datei nicht gefunden, soll ein Error-Dokument geladen werden.
            }
        
            
            //Führe eine Funktion nach dem Laden der View aus
            var fn = window[$(".onViewLoad").data("function")];
            if (typeof fn === 'function') {
                fn();
            } else {
                console.log("[" + view + "] Funktion '" + fn + "' nicht gefunden.");
            }
        });
    
    //Speichere die Ansicht, damit diese beim nächsten Laden automatisch geöffnet wird
    settings_setValue("lastView", view);
}