GFOS Jenkins Team GEW	Version: 0.2

Ein Projekt von Michael M�ller & Sven Morschel zum GFOS Innovationsaward 2015.

-----------------------------------------------
Inhalt dieser "Readme":

1.Installation
2.Benutzungshinweise
3.Lizenzen
-----------------------------------------------

-----------------------------------------------
1. Installation
-----------------------------------------------

Um diese Erweiterung zur Darstellung ihrer Jenkins Prozesse im Browser Google Chrome zu aktivieren gehen Sie bitte folgenderma�en vor: 

1. �ffnen Sie ihren Chrome Browser. 

2. Navigieren Sie in den Erweiterungsbereich ihres Browsers (Google Chrome anpassen und einstellen -> Einstellungen -> Erweiterungen). 

3. Hier aktivieren Sie nun den Entwicklermodus mit Aktivierung des H�ckchens oben rechts.

4. Es erscheinen neue Schaltfl�chen. Zum einen die Schaltfl�che "Entpackte Erweiterungen laden....". Diese klicken Sie an.

5. Navigieren Sie �ber die Auswahl zum Speicherort der Applikationsdateien und w�hlen Sie den Ordner aus, in dem sich diese README.txt-Datei befindet.

6. Google Chrome installiert nun die Applikation automatisch.

-----------------------------------------------
2. Benutzungshinweise
-----------------------------------------------

Sobald Sie die Applikation installiert haben, k�nnen Sie �ber das Jenkins-Icon im oberen rechten Bereich ihres Browsers diese durch einen Mausklick auf das Jenkins-Symbol ausf�hren.
�ber das Zahnrad-Symbol auf der rechten Seite der Navigationsleiste gelangen Sie in den Optionsbereich. Hier k�nnen Sie die Server-URL ihres Jenkins-Severs hinterlegen. Sobald Sie die URL angegeben haben stellt die Applikation eine Verbindung zum Jenkins-Server her. Sie sehen, ob die Verbindung erfolgreich war, wenn die Statusmeldung in der Navigationsleiste Ihnen eben dies mitteilt. Nun k�nnne Sie �ber den Home-Button im linken Bereich der Navigationsleiste ihre Projekte einsehen.

-----------------------------------------------
3. Lizenzen
-----------------------------------------------

Die Jenkins-Logos, die f�r die Applikation genutzt wurden, sind vom Jenkins Projekt und Masanobu Imai unter der Lizenz "Creative Commons Attribution-ShareAlike 3.0 Unported License" zur freien verf�gung gestellt worden. Das Jenkins Projekt finden Sie unter http://jenkins-ci.org/ .

Alle Besitzanspr�che der Applikation werden mit Einreichen des Projektes der GFOS �bertragen.